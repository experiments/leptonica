CFLAGS = -ansi -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS += -fno-common \
	  -Wall \
	  -Wdeclaration-after-statement \
	  -Wextra \
	  -Wformat=2 \
	  -Winit-self \
	  -Winline \
	  -Wpacked \
	  -Wp,-D_FORTIFY_SOURCE=2 \
	  -Wpointer-arith \
	  -Wlarger-than-65500 \
	  -Wmissing-declarations \
	  -Wmissing-format-attribute \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wold-style-definition \
	  -Wredundant-decls \
	  -Wsign-compare \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wswitch-enum \
	  -Wundef \
	  -Wunreachable-code \
	  -Wunsafe-loop-optimizations \
	  -Wunused-but-set-variable \
	  -Wwrite-strings

LDLIBS = -llept

leptonica-projective-transform: leptonica-projective-transform.o

clean:
	rm -rf *~ *.o leptonica-projective-transform transformed_image.jpg

test: leptonica-projective-transform
	valgrind --leak-check=full --show-reachable=yes ./leptonica-projective-transform road.png
